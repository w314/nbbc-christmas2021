# The Very Best Christmas Card Ever

Original by Patrick Rainville Dorn \
Arranged for New Beginnings Baptist Church by James and Katie Anderson


## Table of Contents
[Scene One: Beginning the search for the perfect card.](contents/scene1.md) \
[Scene Two: A Dickens Christmas](contents/scene2.md) \
[Scene Three: Christmas Angels](contents/scene3.md) \
[Scene Four: Christmas Shepherds](contents/scene4.md) \
[Scene Five: The Wise Men](contents/scene5.md) \
[Scene Six: The Nativity](contents/scene6.md) \
[Scene Seven: A Gift of Presence](contents/scene7.md)





## Cast Of Characters
- JENNY: a teen-aged girl (F)
- JEFF: Her younger brother (M)
- SALESPERSON: Works at card shop (F or M)
- MOM: Mother of Jenny and Jeff (F)
- SARAH: Older sister of Jenny and Jeff (F)
- ENSEMBLE: optional, or very flexible cast of about 20


STAGING





There are several ways to stage this play. The simplest would be to create oversized Christmas cards/posters representing the various scenes, and run the show like a short one-act play, with no ensemble, and no singing. The large cards could be placed on small tables as if on display.



* * *



Another way to stage the production would be to have a choir or individual classes set off to the side. The choir or classes can sing the carols, or lead the audience in singing along.



* * *



The most complex production would include an ensemble that performs visual interpretations of the songs as they are being sung, either through improvisation or directed staging. This can be a lot of fun, especially with costume accessories and simple props. In this way, the Christmas cards really come to life!

ABOUT THE MUSIC





Many Christmas carols are public domain, or covered under your organization’s music publisher’s license. However, if you choose to sing a song that is not in the public domain or covered by your organization’s music publisher license, be sure to obtain permission to use the song in your production. With most of these carols, no musical accompaniment is required. However, if resources permit, include at least keyboard accompaniment. You can project the lyrics, or include a sing-along booklet with the program.

Suggested Songs





O Come, All Ye Faithful

Santa Claus is Coming to Town, OR Rudolph the Red-Nosed Reindeer, OR Jolly Old Saint Nicholas, OR other Santa-themed carol.

Deck the Halls, OR We Wish You A Merry Christmas, or other English-themed carol.

Hark! The Herald Angels Sing, OR It Came Upon a Midnight Clear, OR God Rest Ye Merry, Gentlemen, OR another angel-themed carol.

The First Noel, OR While Shepherd’s Watched Their Flocks By Night, OR Angels We Have Heard On High, OR another shepherd-themed carol.

We Three Kings of Orient Are, OR O Come, O Come, Emanuel, OR Come, Thou Long-Expected Jesus, OR another wise men/three kings-themed or Advent-themed carol.

Away in a Manger, Silent Night, OR What Child is This, OR another Nativity-themed carol.

O Little Town of Bethlehem, OR another soft Christmas carol.

Joy to the World
