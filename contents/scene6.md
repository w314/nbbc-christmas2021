# Scene Six

MOM: I think we’re on the right track here, with angels, shepherds, and wise men. Does anyone else see where we’re going?

SARAH: If we are trying to find the perfect card for Min’s first Christmas with us, what message do we really want to share with her? Who was the inspiration for our decision to sponsor her in the first place?

JENNY: Of course! It’s kind of funny that we had to go all the way through these cards to get to the simplest and most obvious choice of all.

MOM: After all, it’s because of Jesus coming into this world that we celebrate Christmas.

JEFF: Christmas is Jesus’ birthday. (ENSEMBLE ENTERS. SONGS: “Away in a Manger,” “Silent Night! Holy Night!” OR “What Child Is This?” ENSEMBLE EXITS.)
