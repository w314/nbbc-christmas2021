# Scene Two


JEFF: I like these Santa cards.

SARAH: They certainly are jolly.

JENNY: Do you think they have Santa Claus in Korea?

MOM: I don’t know. Maybe.

SARAH: That Santa Claus song is kind of scary, when you think about it.

JEFF: We wouldn’t want to scare her. Then she’d be **CLAUS**-trophobic.

JENNY: Oh, brother.

JEFF: That’s me, your ever-loving brother, for better or worse, or whatever.

SARAH: Well, the jokes are getting worse.

MOM: I take it we want to see more cards before we decide which one to buy?

JENNY: That’s right.

SALESPERSON: Fine. Since this orphan you have sponsored is from Korea, maybe she would enjoy seeing something from our own English tradition. (ENSEMBLE ENTERS, strikes a pose.)

SARAH: Yes, this card is very nice. A Charles Dickens Christmas. Hot roasted chestnuts, warm woolen scarves, rosy cheeks, caroling … I love this time of year! (ENSEMBLE ENTERS. SONGS: “Deck and Halls” and “We Wish You A Merry Christmas.” ENSEMBLE EXITS.)
