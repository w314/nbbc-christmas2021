# Scene Three

MOM: Now that’s my idea of a Christmas card. It brings back all kinds of memories of Christmases past.

JENNY: Yes, but do you think Min  will appreciate it in the same way? She lives in a completely different culture.

JEFF: Jenny’s right. (OTHERS look at him in disbelief.) Well, she is! Min doesn’t have the same traditions and memories we have. And this card is for her.

SARAH: You’ve got a point there.

MOM: But those children were so cute. They look like little angels.

SALESPERSON: Angels? Did I hear you say angels? We have this line of angel cards. Eyes lifted to heaven, God’s glory and grandeur, but also peaceful and serene. (ENSEMBLE ENTERS. SONGS: “Hark! The Herald Angels Sing” and “It Came Upon A Midnight Clear” OR “God Rest You, Merry Gentlemen.” ENSEMBLE EXITS.)
