Scene Seven





JENNY: What better card for Min’s first Christmas than a birthday card reminding us of our Savior’s birth? Does everyone agree?

MOM: Yes.

SARAH: It’s perfect.

JENNY: Jeff?

JEFF: Absotively! Posilutely!

SALESPERSON: That’s wonderful. I’ll ring it up for you. But, didn’t you say this card is going all the way to Korea?

JENNY: Yes.

SALESPERSON: Well, I’m afraid there’s no guarantee that it will get there in time.  

JEFF: That’s what we were afraid of.

MOM: What else can we do?

SALESPERSON: Why don't I call Hallmark International in Seoul and ask if they carry this same nativity card?  We can send an e-mail with your personal message and have it enclosed in the card and delivered directly to the orphanage.  I'm sure that we can get it to her in time this way.

JENNY: That's a great idea!  Is it really possible?

SALESPERSON: Of course!  We can send flowers, cards, just about anything around the world the same way.  It costs a little more, of course...

JEFF: But would it get to her on time?

SALESPERSON: In five days?  I'm sure of it.

MOM: Let's do it.  After all, it is Min's first Christmas with us.

SARAH:  And our first Christmas with her.  It's perfect.

JEFF:  Great!

SALESPERSON:  I'll make the arrangements for you right now.

JENNY:  And next year we'll pick out a Christmas card in plenty of time.

JEFF:  Maybe we'd better start shopping now, then.

MOM:  Maybe you're right.

JENNY: A message of our love, with a card celebrating God's eternal love for us all... this really will be the very best Christmas card ever!

(CAST EXITS, singing “Joy to the World!”)