# Scene One

NARRATOR: It is less than a week before Christmas, and a family has realized that they forgot an important part of their gift for the newest member of their family.  With their father \<reason to be gone>, Jenny and Jeff are trying to make it right.

_JENNY and JEFF enter STAGE RIGHT._

JENNY: Are you sure we can get it to her in time? The card has to go all the way to Korea in only five days.

JEFF: When it absotively, posilutely has to get there by Christmas, send it Reindeer Express…

JENNY: Jeff, I’m serious.

JEFF: I really don’t know, Jenny. Sorry.

JENNY: But this will be her first Christmas card. I wanted it to be the very best Christmas card ever.

JEFF: It will be, even if it is a little late. The first thing we need to do is choose a card. Tomorrow we’ll check at the post office about getting the card to Min  before Christmas.

JENNY: I guess you’re right, for once. Let’s concentrate on finding just the right card.

JEFF: Aren’t you forgetting something?

JENNY: What?

JEFF: Mom. You remember her, don’t you? Sorta’ tall, friendly, and drives us around from time to time?  

JENNY: Oh, yeah. Where is she?

JEFF: Last I saw her, she was with Sarah having some gifts wrapped at the department store. Stuff we weren’t supposed to see. (SALESPERSON ENTERS.)

SALESPERSON: Can I help you?

JEFF: Yes, please. We’ve been abandoned by our evil, wicked, cruel and heartless parents. Could you please put us through college?

JENNY: We were just waiting for our folks to catch up with us.

SALESPERSON: Oh, I see. Well, if you need anything, just call me.

JENNY: Thanks. (SALESPERSON EXITS. MOM and SARAH ENTER, pushing a cart that is overflowing with presents.) Here they are now.

MOM: Hi guys. Sorry we’re late. You wouldn’t believe how busy it is out there in the rest of the mall.

SARAH: Have you picked out a nice Christmas card for Min?

JENNY: No, we wanted to wait for you. I think this should be a family decision, don’t you?

MOM: You’re right. It was a family decision to sponsor Min in the first place. And this IS her first Christmas as our foster child.

SARAH: I still can't believe we forgot to send a card along with her present.

JENNY: What we need to do now is choose the best Christmas card for Min that we can, and mail it to the orphanage first thing in the morning. (SALESPERSON ENTERS.) Excuse me.

SALESPERSON: Are you ready now?

SARAH: Yes. We’re looking for a Christmas card.

MOM: It’s for a little orphan girl we are sponsoring.

SALESPERSON: Sponsoring?

JEFF: She’s seven years old. Her parents died, and she has no other family.

MOM: We send money every month to an orphanage in Korea . They feed her, give her a place to live and clothes to wear, and they even have a school for her.

SARAH: This is our first year and we're still figuring everything out.

JEFF: She’s never had a Christmas from us before.

JENNY: So it has to be extra special.

SALESPERSON: We have several kinds of Christmas cards. If you’ll just look over here. Let’s start with our Jolly Old St. Nicholas cards. Ho, ho, ho, Santa will get her into the Christmas spirit! (ENSEMBLE ENTERS, poses in tableau, then leads the sing-along. SONG: “Santa Claus is Coming to Town,” Rudolph the Red Nosed Reindeer,” “Jolly Old St. Nicholas” or other Santa-themed carol. ENSEMBLE EXITS.)
