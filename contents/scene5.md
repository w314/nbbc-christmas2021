# Scene Five

SALESPERSON: What do you think? Have you found the right one?

MOM: I don’t know. We are probably going to end up looking at all of them before we’re finished.

SARAH: They're all very pretty.

JENNY: And we want this to be the very best Christmas card ever for Min.

JEFF: Don’t worry, Jenny. It will be the very best Christmas card for Min this year. But then, it will be the ONLY one from us this year!

JENNY: Jeff, you’re such a wise guy. (ENSEMBLE ENTERS, strikes a pose.)

JEFF: Not me… Them! They’re the wise guys.

SALESPERSON: Oh, I get it. That’s a joke, right?

MOM: Sort of. Don’t encourage him.

SARAH: The wise men. They are the whole basis behind the idea of gift giving at Christmas, because they started the tradition by giving gifts to Baby Jesus. (SONGS: “We Three Kings of Orient Are” and “O Come, O Come, Emmanuel” OR “Come, Thou Long Expected Jesus.” ENSEMBLE EXITS.)
