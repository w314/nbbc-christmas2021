# Scene Four





MOM: Yes, I think this one is just right. It’s got a biblical point of view.

SARAH: That’s true. And Min is not just a member of our family. She’s also a member of God’s family.

JENNY: But an angel doesn’t seem quite right. If she’s a new member of God’s family, a shepherd card might be more appropriate.

MOM: You mean because she is a new lamb in God’s flock? Yes, and we are trying to care for her like the good shepherd. What do you think, Jeff?

JENNY: And please, no baaa-d jokes about sheep.

JEFF: Oh that’s good, Jenny. (To SALESPERSON.) Do you have any shepherd cards?

SALESPERSON: Do I ever!

JEFF: And don’t try to pull the wool over our eyes. We know when someone’s trying to fleece us. (ENSEMBLE ENTERS, strikes a pose.)

JENNY: Can you imagine what it must have been like to have been a shepherd? Out in the hills at night, instead of at home in bed. The sheep huddled close together for warmth, your friends leaning on their staffs, nodding off to sleep under the stars. (SONGS: “The First Noel” and “While Shepherds Watched Their Flocks By Night” OR “Angels We Have Heard On High” ENSEMBLE EXITS.)
