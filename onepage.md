# The Very Best Christmas Card Ever

Original by Patrick Rainville Dorn \
Arranged for New Beginnings Baptist Church by James and Katie Anderson


## Table of Contents
[Scene One: Beginning the search for the perfect card.](contents/scene1.md) \
[Scene Two: A Dickens Christmas](contents/scene2.md) \
[Scene Three: Christmas Angels](contents/scene3.md) \
[Scene Four: Christmas Shepherds](contents/scene4.md) \
[Scene Five: The Wise Men](contents/scene5.md) \
[Scene Six: The Nativity](contents/scene6.md) \
[Scene Seven: A Gift of Presence](contents/scene7.md)





## Cast Of Characters
- JENNY: a teen-aged girl (F)
- JEFF: Her younger brother (M)
- SALESPERSON: Works at card shop (F or M)
- MOM: Mother of Jenny and Jeff (F)
- SARAH: Older sister of Jenny and Jeff (F)
- ENSEMBLE: optional, or very flexible cast of about 20


STAGING





There are several ways to stage this play. The simplest would be to create oversized Christmas cards/posters representing the various scenes, and run the show like a short one-act play, with no ensemble, and no singing. The large cards could be placed on small tables as if on display.



* * *



Another way to stage the production would be to have a choir or individual classes set off to the side. The choir or classes can sing the carols, or lead the audience in singing along.



* * *



The most complex production would include an ensemble that performs visual interpretations of the songs as they are being sung, either through improvisation or directed staging. This can be a lot of fun, especially with costume accessories and simple props. In this way, the Christmas cards really come to life!

ABOUT THE MUSIC





Many Christmas carols are public domain, or covered under your organization’s music publisher’s license. However, if you choose to sing a song that is not in the public domain or covered by your organization’s music publisher license, be sure to obtain permission to use the song in your production. With most of these carols, no musical accompaniment is required. However, if resources permit, include at least keyboard accompaniment. You can project the lyrics, or include a sing-along booklet with the program.

Suggested Songs





O Come, All Ye Faithful

Santa Claus is Coming to Town, OR Rudolph the Red-Nosed Reindeer, OR Jolly Old Saint Nicholas, OR other Santa-themed carol.

Deck the Halls, OR We Wish You A Merry Christmas, or other English-themed carol.

Hark! The Herald Angels Sing, OR It Came Upon a Midnight Clear, OR God Rest Ye Merry, Gentlemen, OR another angel-themed carol.

The First Noel, OR While Shepherd’s Watched Their Flocks By Night, OR Angels We Have Heard On High, OR another shepherd-themed carol.

We Three Kings of Orient Are, OR O Come, O Come, Emanuel, OR Come, Thou Long-Expected Jesus, OR another wise men/three kings-themed or Advent-themed carol.

Away in a Manger, Silent Night, OR What Child is This, OR another Nativity-themed carol.

O Little Town of Bethlehem, OR another soft Christmas carol.

Joy to the World
# Scene One

NARRATOR: It is less than a week before Christmas, and a family has realized that they forgot an important part of their gift for the newest member of their family.  With their father \<reason to be gone>, Jenny and Jeff are trying to make it right.

_JENNY and JEFF enter STAGE RIGHT._

JENNY: Are you sure we can get it to her in time? The card has to go all the way to Korea in only five days.

JEFF: When it absotively, posilutely has to get there by Christmas, send it Reindeer Express…

JENNY: Jeff, I’m serious.

JEFF: I really don’t know, Jenny. Sorry.

JENNY: But this will be her first Christmas card. I wanted it to be the very best Christmas card ever.

JEFF: It will be, even if it is a little late. The first thing we need to do is choose a card. Tomorrow we’ll check at the post office about getting the card to Min  before Christmas.

JENNY: I guess you’re right, for once. Let’s concentrate on finding just the right card.

JEFF: Aren’t you forgetting something?

JENNY: What?

JEFF: Mom. You remember her, don’t you? Sorta’ tall, friendly, and drives us around from time to time?  

JENNY: Oh, yeah. Where is she?

JEFF: Last I saw her, she was with Sarah having some gifts wrapped at the department store. Stuff we weren’t supposed to see. (SALESPERSON ENTERS.)

SALESPERSON: Can I help you?

JEFF: Yes, please. We’ve been abandoned by our evil, wicked, cruel and heartless parents. Could you please put us through college?

JENNY: We were just waiting for our folks to catch up with us.

SALESPERSON: Oh, I see. Well, if you need anything, just call me.

JENNY: Thanks. (SALESPERSON EXITS. MOM and SARAH ENTER, pushing a cart that is overflowing with presents.) Here they are now.

MOM: Hi guys. Sorry we’re late. You wouldn’t believe how busy it is out there in the rest of the mall.

SARAH: Have you picked out a nice Christmas card for Min?

JENNY: No, we wanted to wait for you. I think this should be a family decision, don’t you?

MOM: You’re right. It was a family decision to sponsor Min in the first place. And this IS her first Christmas as our foster child.

SARAH: I still can't believe we forgot to send a card along with her present.

JENNY: What we need to do now is choose the best Christmas card for Min that we can, and mail it to the orphanage first thing in the morning. (SALESPERSON ENTERS.) Excuse me.

SALESPERSON: Are you ready now?

SARAH: Yes. We’re looking for a Christmas card.

MOM: It’s for a little orphan girl we are sponsoring.

SALESPERSON: Sponsoring?

JEFF: She’s seven years old. Her parents died, and she has no other family.

MOM: We send money every month to an orphanage in Korea . They feed her, give her a place to live and clothes to wear, and they even have a school for her.

SARAH: This is our first year and we're still figuring everything out.

JEFF: She’s never had a Christmas from us before.

JENNY: So it has to be extra special.

SALESPERSON: We have several kinds of Christmas cards. If you’ll just look over here. Let’s start with our Jolly Old St. Nicholas cards. Ho, ho, ho, Santa will get her into the Christmas spirit! (ENSEMBLE ENTERS, poses in tableau, then leads the sing-along. SONG: “Santa Claus is Coming to Town,” Rudolph the Red Nosed Reindeer,” “Jolly Old St. Nicholas” or other Santa-themed carol. ENSEMBLE EXITS.)
# Scene Two


JEFF: I like these Santa cards.

SARAH: They certainly are jolly.

JENNY: Do you think they have Santa Claus in Korea?

MOM: I don’t know. Maybe.

SARAH: That Santa Claus song is kind of scary, when you think about it.

JEFF: We wouldn’t want to scare her. Then she’d be **CLAUS**-trophobic.

JENNY: Oh, brother.

JEFF: That’s me, your ever-loving brother, for better or worse, or whatever.

SARAH: Well, the jokes are getting worse.

MOM: I take it we want to see more cards before we decide which one to buy?

JENNY: That’s right.

SALESPERSON: Fine. Since this orphan you have sponsored is from Korea, maybe she would enjoy seeing something from our own English tradition. (ENSEMBLE ENTERS, strikes a pose.)

SARAH: Yes, this card is very nice. A Charles Dickens Christmas. Hot roasted chestnuts, warm woolen scarves, rosy cheeks, caroling … I love this time of year! (ENSEMBLE ENTERS. SONGS: “Deck and Halls” and “We Wish You A Merry Christmas.” ENSEMBLE EXITS.)
# Scene Three

MOM: Now that’s my idea of a Christmas card. It brings back all kinds of memories of Christmases past.

JENNY: Yes, but do you think Min  will appreciate it in the same way? She lives in a completely different culture.

JEFF: Jenny’s right. (OTHERS look at him in disbelief.) Well, she is! Min doesn’t have the same traditions and memories we have. And this card is for her.

SARAH: You’ve got a point there.

MOM: But those children were so cute. They look like little angels.

SALESPERSON: Angels? Did I hear you say angels? We have this line of angel cards. Eyes lifted to heaven, God’s glory and grandeur, but also peaceful and serene. (ENSEMBLE ENTERS. SONGS: “Hark! The Herald Angels Sing” and “It Came Upon A Midnight Clear” OR “God Rest You, Merry Gentlemen.” ENSEMBLE EXITS.)
# Scene Four





MOM: Yes, I think this one is just right. It’s got a biblical point of view.

SARAH: That’s true. And Min is not just a member of our family. She’s also a member of God’s family.

JENNY: But an angel doesn’t seem quite right. If she’s a new member of God’s family, a shepherd card might be more appropriate.

MOM: You mean because she is a new lamb in God’s flock? Yes, and we are trying to care for her like the good shepherd. What do you think, Jeff?

JENNY: And please, no baaa-d jokes about sheep.

JEFF: Oh that’s good, Jenny. (To SALESPERSON.) Do you have any shepherd cards?

SALESPERSON: Do I ever!

JEFF: And don’t try to pull the wool over our eyes. We know when someone’s trying to fleece us. (ENSEMBLE ENTERS, strikes a pose.)

JENNY: Can you imagine what it must have been like to have been a shepherd? Out in the hills at night, instead of at home in bed. The sheep huddled close together for warmth, your friends leaning on their staffs, nodding off to sleep under the stars. (SONGS: “The First Noel” and “While Shepherds Watched Their Flocks By Night” OR “Angels We Have Heard On High” ENSEMBLE EXITS.)
# Scene Five

SALESPERSON: What do you think? Have you found the right one?

MOM: I don’t know. We are probably going to end up looking at all of them before we’re finished.

SARAH: They're all very pretty.

JENNY: And we want this to be the very best Christmas card ever for Min.

JEFF: Don’t worry, Jenny. It will be the very best Christmas card for Min this year. But then, it will be the ONLY one from us this year!

JENNY: Jeff, you’re such a wise guy. (ENSEMBLE ENTERS, strikes a pose.)

JEFF: Not me… Them! They’re the wise guys.

SALESPERSON: Oh, I get it. That’s a joke, right?

MOM: Sort of. Don’t encourage him.

SARAH: The wise men. They are the whole basis behind the idea of gift giving at Christmas, because they started the tradition by giving gifts to Baby Jesus. (SONGS: “We Three Kings of Orient Are” and “O Come, O Come, Emmanuel” OR “Come, Thou Long Expected Jesus.” ENSEMBLE EXITS.)
# Scene Six

MOM: I think we’re on the right track here, with angels, shepherds, and wise men. Does anyone else see where we’re going?

SARAH: If we are trying to find the perfect card for Min’s first Christmas with us, what message do we really want to share with her? Who was the inspiration for our decision to sponsor her in the first place?

JENNY: Of course! It’s kind of funny that we had to go all the way through these cards to get to the simplest and most obvious choice of all.

MOM: After all, it’s because of Jesus coming into this world that we celebrate Christmas.

JEFF: Christmas is Jesus’ birthday. (ENSEMBLE ENTERS. SONGS: “Away in a Manger,” “Silent Night! Holy Night!” OR “What Child Is This?” ENSEMBLE EXITS.)
Scene Seven





JENNY: What better card for Min’s first Christmas than a birthday card reminding us of our Savior’s birth? Does everyone agree?

MOM: Yes.

SARAH: It’s perfect.

JENNY: Jeff?

JEFF: Absotively! Posilutely!

SALESPERSON: That’s wonderful. I’ll ring it up for you. But, didn’t you say this card is going all the way to Korea?

JENNY: Yes.

SALESPERSON: Well, I’m afraid there’s no guarantee that it will get there in time.  

JEFF: That’s what we were afraid of.

MOM: What else can we do?

SALESPERSON: Why don't I call Hallmark International in Seoul and ask if they carry this same nativity card?  We can send an e-mail with your personal message and have it enclosed in the card and delivered directly to the orphanage.  I'm sure that we can get it to her in time this way.

JENNY: That's a great idea!  Is it really possible?

SALESPERSON: Of course!  We can send flowers, cards, just about anything around the world the same way.  It costs a little more, of course...

JEFF: But would it get to her on time?

SALESPERSON: In five days?  I'm sure of it.

MOM: Let's do it.  After all, it is Min's first Christmas with us.

SARAH:  And our first Christmas with her.  It's perfect.

JEFF:  Great!

SALESPERSON:  I'll make the arrangements for you right now.

JENNY:  And next year we'll pick out a Christmas card in plenty of time.

JEFF:  Maybe we'd better start shopping now, then.

MOM:  Maybe you're right.

JENNY: A message of our love, with a card celebrating God's eternal love for us all... this really will be the very best Christmas card ever!

(CAST EXITS, singing “Joy to the World!”)